//В файле index.html лежит разметка для кнопок.
//Каждая кнопка содержит в себе название клавиши на клавиатуре
//По нажатию указанных клавиш - та кнопка, на которой написана эта буква, должна окрашиваться в синий цвет. При этом, если какая-то другая буква уже ранее была окрашена в синий цвет - она становится черной. Например по нажатию Enter первая кнопка окрашивается в синий цвет. Далее, пользователь нажимает S, и кнопка S окрашивается в синий цвет, а кнопка Enter опять становится черной.

const allButtons = document.querySelectorAll('.btn');

function setStyleBlackAll (){
    allButtons.forEach(function (element){
        element.style.background = "black"
    })
}

document.addEventListener('keyup', (event)=>{
    for (let element of allButtons){
        if (event.code === element.dataset.name){
            setStyleBlackAll();
            element.style.background = "blue";
        }
    }
})